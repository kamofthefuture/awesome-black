# blcklbrtn

A repository for ideas and solutions centering blackness and black lives. 

## The Goal
The main goal for this project is to be a public space and record of project made and/ or deeply supported/ centered by black people. Nothing can be too big or too small please share stores, clothing lines, apps, poetry books, etc.
The second focus within this repo is to incubate ideas and grow talent, confidence and trust within our community.

## Contributing
As ideas come to your mind for supporting the call for black liberation please create issues or make a PR to add to the README,

## Crediting
All things added into this space must credit with the full name of the creators (dont fucking erase us)

## Ideas
If you are adding an idea you should not have the expectation that it will automatically happen, as ideas come we can work on teams and reach out to folx willing to contribute

# The good stuff - oh theres a lot of good stuff

Buy black
- [reparations club](https://rep.club) - Reparations Club is a concept shop & creative space curated by Blackness & POC in Los Angeles, CA.
- [Akilogram](https://heyakilogram.com) - Clothing brand, creative / cultural consultation
- [Artsywindow](https://www.artsywindow.com) - Art catologues, curatorial work, etc.
- [Business Name](https://google.com) - 

Hire black talent
- [Kiara Ventura - curating | art writing | art portfolio and application reviews | art advisory | panels and conversations |art consulting and research | events and programming](https://kiaraventura.com)
- [Felix A - Artist, Director, Editor & Softcore VFX](http://Www.vimeo.com/bemiworks)
- [Akile Butler - Embroidrist, visualist, & artist](https://www.instagram.com/akilogram/)
- [Joygill Moriah - Curator at Instagram | Insights | Trends | Data-driven | Culture & Strategy](https://www.linkedin.com/in/joygill-moriah-b4b24657/)
- [Marc Brown - Software Engineer](https://www.linkedin.com/in/signedbymarc/)
- [Name - talents](http://google.com/)